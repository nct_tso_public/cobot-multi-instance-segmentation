# Cobot Multi-Instance Segmentation



## Getting started
This repository contains the python code used to train and evaluate the models in the paper "Artificial Intelligence for context-aware surgical guidance in complex robot-assisted oncological procedures: An exploratory feasibility study". It expects the following data structure
```
data
|___LM
    |___OR2
    |   image00.png
    |   mask00.png
    |   image01.png
    |   mask01.png
    |   ...
    |___OR8
    |___OR10
    |___OR12
    |___OR13
    |___OR14
    |___OR15
    |___OR16
    |___OR18
    |___OR19
|___ME
    |___OR1
    |___OR2
    |___OR5
    |___OR6
    |___OR7
    |___OR8
    |___OR11
    |___OR14
    |___OR16
    |___OR17
|___MM
    |___OR2
    |___OR3
    |___OR4
    |___OR5
    |___OR6
    |___OR7
    |___OR8
    |___OR12
    |___OR13
    |___OR14
    |___OR19
|___VD
    |___OR2
    |___OR8
    |___OR9
    |___OR10
    |___OR12
    |___OR18
    |___OR14
    |___OR16
    |___OR17
    |___OR13"
```