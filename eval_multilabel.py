from cmath import nan
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from torchvision import models
import torch
import torchvision.transforms as transforms
import dataloader
import os
import torch.nn as nn
import torch.cuda.amp
import numpy as np
import cv2
import sys
import albumentations as A
import albumentations.augmentations.functional as F
from albumentations.pytorch import ToTensorV2
from tqdm import tqdm

#TODO: Set correct values for your machine
data_folder = "TODO"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)
mixed_precision = True
epochs = 100

phases = {}
phases["VD"] = ["OR2",
                "OR8",
                "OR9",
                "OR10",
                "OR12",
                "OR13",
                "OR14",
                "OR16",
                "OR17",
                "OR18"]

phases["MM"] = ["OR2",
                "OR3",
                "OR4",
                "OR5",
                "OR6",
                "OR7",
                "OR8",
                "OR12",
                "OR13",
                "OR14",
                "OR19"]
			
phases["ME"] = ["OR1",
                "OR2",
                "OR5",
                "OR6",
                "OR7",
                "OR8",
                "OR11",
                "OR14",
                "OR16",
                "OR17"]
			
phases["LM"] = ["OR2",
                "OR8",
                "OR10",
                "OR12",
                "OR13",
                "OR14",
                "OR15",
                "OR16",
                "OR18",
                "OR19"]

phase_clases = {"VD" : 4, "MM" : 4, "LM" : 5, "ME" : 4}
phase_class_names = {"LM" : ["Abdominal Wall", "Adhesion", "Colon", "Fat", "Small Intestine"],
                    "ME" : ["Dissection plane (ME)", "Dissection line (ME)", "Rectum", "Seminal vesicles"],
                    "MM" : ["Gerota's Fascia", "Mesocolon", "Dissection line (MM)", "Exploration area"],
                    "VD" : ["Inferior mesenteric artery", "Inferior mesenteric vein", "Plastic clip", "Metal clip"]}

start = 0
stop = 4

val_ids = [[], [], [], []]

phase = sys.argv[1]

preds_collection = {}

for i in range(len(phases[phase])):
    val_ids[i % 4].append(phases[phase][i])
    preds_collection[phases[phase][i]] = {}
    for j in phase_class_names[phase]:
        preds_collection[phases[phase][i]][j] = []

num_classes = phase_clases[phase] + 1

test_batches = 0
f1 = []
iou = []
precision = []
recall = []
specificity = []

for i in range(num_classes -1):
    f1.append([])
    iou.append([])
    precision.append([])
    recall.append([])
    specificity.append([])

batch_size = 32
mini_batch_size = 32
num_mini_batches = batch_size//mini_batch_size

image_size = (640, 512)

def metrics(labels, pred, lbl):
    ind_found = pred == lbl
    ind_not_found = pred != lbl
    ind_present = labels == lbl
    ind_not_present = labels != lbl

    tp = np.sum(ind_found*ind_present)
    fp = np.sum(ind_found*ind_not_present)
    fn = np.sum(ind_not_found*ind_present)
    tn = np.sum(ind_not_found*ind_not_present)

    if tp+fn+fp > 0:
        f1 = tp/(tp + 0.5*(fp + fn))
        jac = tp/(tp+fn+fp)
    else:
        f1 = nan
        jac = nan
    if tp + fp > 0:
        p = tp/(tp+fp)
    else:
        p = nan
    if tp + fn > 0:
        r = tp/(tp+fn)
    else:
        r = nan
    if tn+fp > 0:
        s = tn/(tn+fp)
    else:
        s = nan
    
    return f1, jac, p, r, s

for val_id in range(start, stop):
    files = []
    model_path = sys.argv[2 + val_id]
    print(model_path)

    model = models.segmentation.deeplabv3_resnet50(pretrained=True, progress=True)
    model.classifier = DeepLabHead(2048, num_classes)
    model = nn.DataParallel(model)
    model.load_state_dict(torch.load(model_path))
    model.eval()
    model.to(device)

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

    val_transform = A.Compose(
    [
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])

    inv_normalize = transforms.Normalize(
        mean=[-0.485/0.229, -0.456/0.224, -0.406/0.255],
        std=[1/0.229, 1/0.224, 1/0.255]
    )

   
    cur_data_folder = data_folder + phase + "/"

    val_sets = []

    print("Loading data")

    for x in os.walk(cur_data_folder):
        val = False

        if not os.path.isfile(x[0] + "/image00.png"):
            continue
        
        for id in val_ids[val_id]:
            if id in x[0]:
                val = True
                break        

        if val:
            dataset = dataloader.CobotLoaderMulti(x[0], num_classes, val_transform, image_size=image_size)
            val_sets.append(dataset)
            files += dataset.files
          
    val_sets = torch.utils.data.ConcatDataset(val_sets)
    val_loader = torch.utils.data.DataLoader(val_sets, batch_size=mini_batch_size, shuffle=False)

    count = 0

    with torch.no_grad():
        for img, lbl in tqdm(val_loader):
            preds = []
            labels = []
            img = img.to(device)
            lbl = lbl.to(device).long()

            with torch.cuda.amp.autocast(enabled=mixed_precision):
                outputs = model(img)
                pred = torch.argmax(outputs['out'], 1)

                for j in range(pred.size(0)):
                    preds.append(np.ndarray.flatten(pred[j].cpu().numpy()))
                    labels.append(np.ndarray.flatten(lbl[j].cpu().numpy()))

            for i in range(len(preds)):

                for j in range(1, num_classes):
                    f, jac, p, r, s = metrics(labels[i], preds[i], j)
                    
                    if f == f:
                        f1[j-1].append(f)
                    if p == p:
                        precision[j-1].append(p)
                    if r == r:
                        recall[j-1].append(r)
                    if jac == jac:
                        iou[j-1].append(jac)
                    if s == s:
                        specificity[j-1].append(s)

                    tid = ""

                    for t in val_ids[val_id]:
                        if ("/" + t + "/") in files[count]:
                            tid = t
                    assert not tid == ""

                    preds_collection[tid][phase_class_names[phase][j-1]].append((jac, r, p, f, files[count], phase_class_names[phase][j-1], s, (pred[i] == j).cpu().numpy().astype(np.uint8)))

                count+= 1
f = open("Results_%s.csv" % phase, "w")
f.write("File, class, f1, recall, precision, iou, specificity\n")

new_collection = {}
for p in preds_collection:
    for n in preds_collection[p]:
        for i in range(len(preds_collection[p][n])):
            f.write(preds_collection[p][n][i][4] + "," + preds_collection[p][n][i][5] + ",%f,%f,%f,%f,%f\n" % (preds_collection[p][n][i][3], preds_collection[p][n][i][1], preds_collection[p][n][i][2], preds_collection[p][n][i][0], preds_collection[p][n][i][6]))
            if preds_collection[p][n][i][0] == preds_collection[p][n][i][0]:
                if not p in new_collection:
                    new_collection[p] = {}
                if not n in new_collection[p]:
                    new_collection[p][n] = []
                new_collection[p][n].append(preds_collection[p][n][i])
f.close()


print("\nF1,IoU,Precision,Recall,Specificity,")
for i in range(num_classes-1):
    f_m = np.mean(f1[i])
    f_s = np.std(f1[i])
    r_m = np.mean(recall[i])
    r_s = np.std(recall[i])
    p_m = np.mean(precision[i])
    p_s = np.std(precision[i])
    i_m = np.mean(iou[i])
    i_s = np.std(iou[i])
    s_m = np.mean(specificity[i])
    s_s = np.std(specificity[i])

    str = "%.2f pm %.2f," % (f_m, f_s)
    str += "%.2f pm %.2f," % (i_m, i_s)
    str += "%.2f pm %.2f," % (p_m, p_s)
    str += "%.2f pm %.2f," % (r_m, r_s)
    str += "%.2f pm %.2f," % (s_m, s_s)

    print(str)

def tsort(elem):
    return elem[0]

for tid in phases[phase]:
    if not tid in new_collection:
        continue
    for j in range(len(phase_class_names[phase])):
        seg = phase_class_names[phase][j]

        if not seg in new_collection[tid]:
            continue
        data = new_collection[tid][seg]
        if len(data) == 0:
            continue

        data.sort(key=tsort)

        for i in range (1):
            elem_best = data[-1 - i]
            elem_worse = data[i]
            
            img = cv2.imread(elem_best[4]).astype(np.int32)
            img = img[28:28+1024,320:320+1280]
            
            orig_mask = cv2.imread(elem_best[4].replace("image", "mask"), cv2.IMREAD_GRAYSCALE)
            orig_mask = orig_mask[28:28+1024,320:320+1280]
            orig_mask = orig_mask == (j+1)
            orig_mask = orig_mask.astype(np.uint8)
            
            contours, hierarchy = cv2.findContours(orig_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            print(len(contours))
            img_contours = np.copy(img)

            cv2.drawContours(img_contours, contours, -1, (152, 90, 19), 4)
            cv2.imwrite(phase + "_" + tid + "_" + seg + "_best%d_orig.png" % i, img_contours)

            mask = cv2.resize(elem_best[-1], (img.shape[1], img.shape[0]), interpolation=cv2.INTER_NEAREST).astype(bool)

            img[mask] = (img[mask] + 255)//2
            img = img.astype(np.uint8)

            cv2.imwrite(phase + "_" + tid + "_" + seg + "_best%d.png" % i, img)

            img = cv2.imread(elem_worse[4]).astype(np.int32)
            img = img[28:28+1024,320:320+1280]

            orig_mask = cv2.imread(elem_worse[4].replace("image", "mask"), cv2.IMREAD_GRAYSCALE)
            orig_mask = orig_mask[28:28+1024,320:320+1280]
            orig_mask = orig_mask == (j+1)
            orig_mask = orig_mask.astype(np.uint8)

            contours, hierarchy = cv2.findContours(orig_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            img_contours = np.copy(img)

            cv2.drawContours(img_contours, contours, -1, (152, 90, 19), 4)
            cv2.imwrite(phase + "_" + tid + "_" + seg + "_worse%d_orig.png" % i, img_contours)

            mask = cv2.resize(elem_worse[-1], (img.shape[1], img.shape[0]), interpolation=cv2.INTER_NEAREST).astype(bool)

            img[mask] = (img[mask] + 255)//2
            img = img.astype(np.uint8)

            cv2.imwrite(phase + "_" + tid + "_" + seg + "_worse%d.png" % i, img)