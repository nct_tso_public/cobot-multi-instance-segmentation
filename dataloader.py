import os
from torch.utils.data import Dataset
import numpy as np
import cv2
import multiprocessing

class CobotLoaderMulti(Dataset):

    def __init__(self, root_dir, num_labels, transform, image_size=None):
        self.root_dir = root_dir
        self.images = []
        self.labels = []
        self.weights = []

        self.transform = transform

        self.num_pixels = 0
        self.num_bg_pixels = 0
        self.files = []
        self.a_masks = []

        self.cl_weights = np.zeros(num_labels)
        self.image_size = image_size
        self.num_labels = num_labels

        image_count = {}

        for i in range(num_labels+1):
            image_count[i] = 0

        pool = multiprocessing.Pool(8)
    
        for file in os.listdir(root_dir):
            if "png" in file and file[0:5] == "image": 
                
                file = os.path.join(root_dir, file)
                self.files.append(file)

        self.files.sort()
        self.images, self.labels, self.a_masks, weights = zip(*pool.map(self.read_data, self.files))

        for j in range(len(self.images)):
            for i in range(num_labels+1):
                if np.any(self.labels[j] == i):
                    image_count[i] += 1
        
        str = ""
        for i in range(num_labels+1):
            str+= "%d," % image_count[i]
        print(str)

        pool.close() # no more tasks
        pool.join()  # wrap up current tasks

        for w in weights:
            self.cl_weights += w

        self.num_pixels = np.sum(self.cl_weights)
        self.num_bg_pixels = self.cl_weights[0]

    
    def read_data(self, file):
        img = cv2.imread(file)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)     
        cl_weights = np.zeros(self.num_labels)     

        img = img[28:28+1024,320:320+1280]
        if self.image_size is not None:
            img = cv2.resize(img,self.image_size)
        
        mask_file = file.replace("image", "mask")
        mask_orig = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        
        mask_orig = mask_orig[28:28+1024,320:320+1280]
        
        if self.image_size is not None:
            mask_orig = cv2.resize(mask_orig,self.image_size, interpolation=cv2.INTER_NEAREST)
                            
        mask = mask_orig

        for i in range(self.num_labels):
            cl_weights[i] += np.sum(mask == i)
        
        return img, mask, np.copy(mask), cl_weights

    def get_frequency(self):        
        return self.cl_weights

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        if self.transform is None:
            return self.images[idx], self.labels[idx]

        transformed = self.transform(image=self.images[idx], mask=self.labels[idx])
        
        img = transformed["image"]
        mask = transformed["mask"]

        return img, mask