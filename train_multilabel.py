import datetime
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from torchvision import models
import torch
import torchvision.transforms as transforms
import dataloader
import os
import torch.nn as nn
import torch.optim as optim
import time
import torch.cuda.amp
import numpy as np
import cv2
import sys
import albumentations as A
import albumentations.augmentations.functional as F
import torch.nn.functional as F2
from albumentations.pytorch import ToTensorV2
from tqdm import tqdm

#TODO: Set correct values for your machine
output_folder = "TODO"
data_folder = "TODO"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

epochs = 500

phases = {}
phases["VD"] = ["OR2",
                "OR8",
                "OR9",
                "OR10",
                "OR12",
                "OR18",
                "OR14",
                "OR16",
                "OR17",
                "OR13"
                ]

phases["MM"] = ["OR2",
                "OR3",
                "OR4",
                "OR5",
                "OR6",
                "OR7",
                "OR8",
                "OR12",
                "OR13",
                "OR14",
                "OR19"]
			
phases["ME"] = ["OR1",
                "OR2",
                "OR5",
                "OR6",
                "OR7",
                "OR8",
                "OR11",
                "OR14",
                "OR16",
                "OR17"]
			
phases["LM"] = ["OR2",
                "OR8",
                "OR10",
                "OR12",
                "OR13",
                "OR14",
                "OR15",
                "OR16",
                "OR18",
                "OR19"]

phase_clases = {"VD" : 4, "MM" : 4, "LM" : 5, "ME" : 4}

start = 0
stop = 4

val_ids = [[], [], [], []]

phase = sys.argv[1]
if len(sys.argv) > 2:
    start = int(sys.argv[2])
    stop = start + 1
for i in range(len(phases[phase])):
    val_ids[i % 4].append(phases[phase][i])

num_classes = phase_clases[phase] + 1


mixed_precision = False
batch_size = 16
mini_batch_size = 16
num_mini_batches = batch_size//mini_batch_size

image_size = (640, 512)

for val_id in range(start, stop):
    model = models.segmentation.deeplabv3_resnet50(pretrained=True, progress=True)
    model.classifier = DeepLabHead(2048, num_classes)

    model = nn.DataParallel(model)
    model.train()
    model.to(device)
    cur_output_folder = output_folder + phase + "_%d_" % val_id
    cur_output_folder += datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + "/"
    os.makedirs(cur_output_folder, exist_ok=True)

    train_transform = A.Compose(
    [
        A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=5, p=0.5),
        A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.5),
        A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=0.5),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])

    val_transform = A.Compose(
    [
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])

    cur_data_folder = data_folder + phase + "/"

    train_sets = []
    val_sets = []

    print("Loading data")

    def init_f1(list, num_labels):
        for i in range(num_labels):
            list.append([0,0,0,[],[]])

    def update_f1(list, pred, lbl, num_labels):
        for i in range(pred.size(0)):
            for j in range(num_labels):
                label =j
                tp = torch.sum((pred[i] == label)*(lbl[i] == label)).item()
                fp = torch.sum((pred[i] == label)*(lbl[i] != label)).item()
                fn = torch.sum((pred[i] != label)*(lbl[i] == label)).item()
                tn = torch.sum((pred[i] != label)*(lbl[i] != label)).item()

                list[label][0] += tp
                list[label][1] += fp
                list[label][2] += fn
                if (tp + fp + fn) > 0:
                    f1 = tp/(tp + 0.5*(fp + fn))
                    jc = tp/(tp + fp + fn)
                    list[label][3].append(f1)
                    list[label][4].append(jc)

    def compute_avg_f1(list, ignore_zero_label=True):
        f1s = []
        f1s2 = []
        prs = []
        rcs = []
        jcs = []
        jcs2 = []

        str = "("
        str2 = "("

        for i in range(len(list)):
            if i == 0 and ignore_zero_label:
                continue

            tp = list[i][0]
            fp = list[i][1]
            fn = list[i][2]

            if (tp + fp + fn) > 0:
                f1 = tp/(tp + 0.5*(fp + fn))
                jc = tp/(tp + fp + fn)

                f1s.append(f1)
                jcs.append(jc)
                if (fp + tp) > 0:
                    prs.append(tp/(fp + tp))
                if (fn + tp) > 0:
                    rcs.append(tp/(tp+fn))
            f1s2.append(np.mean(list[i][3]))
            jcs2.append(np.mean(list[i][4]))
            str += "%.3f," % f1s2[-1]
            str2 += "%.3f," % jcs2[-1]
                
        str = str[:-1] + ")"
        str2 = str2[:-1] + ")"
        print(str,str2)
        return np.nanmean(f1s), np.nanmean(prs), np.nanmean(rcs), np.nanmean(jcs), np.nanmean(f1s2), np.nanmean(jcs2)

    weights = np.zeros(num_classes, dtype=np.float32)

    for x in os.walk(cur_data_folder):
        val = False

        if not os.path.isfile(x[0] + "/image00.png"):
            continue
        
        for id in val_ids[val_id]:
            if id in x[0]:
                val = True
                break      

        if val:
            dataset = dataloader.CobotLoaderMulti(x[0], num_classes, val_transform, image_size=image_size)
            val_sets.append(dataset)
        else:
            dataset = dataloader.CobotLoaderMulti(x[0], num_classes, train_transform, image_size=image_size)
            train_sets.append(dataset)
            cl_weights = dataset.get_frequency()
            weights += cl_weights
            
    print(weights)
    n_samples = np.sum(weights)

    weights = 1 - weights*1/np.sum(weights)
    weights = weights.astype(np.float16)
    print(weights)

    train_sets = torch.utils.data.ConcatDataset(train_sets)
    val_sets = torch.utils.data.ConcatDataset(val_sets)

    train_loader = torch.utils.data.DataLoader(train_sets, batch_size=mini_batch_size, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_sets, batch_size=mini_batch_size, shuffle=False)

    criterion = nn.CrossEntropyLoss()
    lr = 1e-4

    optimizer = optim.AdamW(filter(lambda p: p.requires_grad, model.parameters()), lr=lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer, 20, 0.9, verbose=True)

    scaler = torch.cuda.amp.GradScaler(enabled=True)

    log_file = open(cur_output_folder + "/log.txt", "w")
    print("Training model")
    for e in range(epochs):
        optimizer.zero_grad()
        train_batches = 0

        model.train()
        train_loss = []
        test_loss = []
        train_accuracy = []
        test_accuracy = []
        train_f1 = []
        test_f1 = []

        init_f1(train_f1, num_classes)
        init_f1(test_f1, num_classes)

        t0 = time.time()
        for img, lbl in tqdm(train_loader):
            if img.size(0) < mini_batch_size:
                continue
            img = img.to(device)
            lbl = lbl.to(device).long()

            with torch.cuda.amp.autocast(enabled=mixed_precision):
                outputs = model(img)['out']
            
                loss = criterion(outputs, lbl)
                
                pred = torch.argmax(outputs, 1)

                update_f1(train_f1, pred, lbl, num_classes)

                train_loss.append(loss.item())
                train_accuracy.append(torch.sum(pred == lbl).item()/(mini_batch_size*image_size[0]*image_size[1]))

            if mixed_precision:
                scaler.scale(loss).backward()
            else:
                loss.backward()

            train_batches += 1

            if train_batches % num_mini_batches == 0:
                if mixed_precision:
                    scaler.step(optimizer)
                    scaler.update()
                else:
                    optimizer.step()
            
        if train_batches % num_mini_batches != 0:
            if mixed_precision:
                scaler.step(optimizer)
                scaler.update()
            else:
                optimizer.step()
        
        model.eval()
        preds = []
        run_times = []
        test_batches = 0
        with torch.no_grad():
            for img, lbl in tqdm(val_loader):
                t_start = time.time()
                img = img.to(device)
                lbl = lbl.to(device).long()
   
                with torch.cuda.amp.autocast(enabled=mixed_precision):
                    outputs = model(img)['out']
                    loss = criterion(outputs, lbl)
                    pred = torch.argmax(outputs, 1)
                    preds.append(pred.cpu().numpy())

                    update_f1(test_f1, pred, lbl, num_classes)

                    test_loss.append(loss.item())
                    test_accuracy.append(torch.sum(pred == lbl).item()/(mini_batch_size*image_size[0]*image_size[1]))
                t_stop = time.time()
                run_times.append(t_stop - t_start)            
            test_batches += 1
        
        t1 = time.time()

        preds = np.concatenate(preds,axis=0)
        unique, counts = np.unique(preds, return_counts=True)

        total = t1-t0

        train_f1, train_pr, train_rc, train_jac, train_f1_2, train_jac_2 = compute_avg_f1(train_f1)
        test_f1, test_pr, test_rc, test_jac, test_f1_2, test_jac_2 = compute_avg_f1(test_f1)
        val_fps = 1/np.mean(run_times)
        str = "Epoch %d: Train (loss %.3f accuracy %.3f f1 %.3f (f1 %.3f) pr %.3f rc %.3f jac %.3f (jac %.3f ) Test (loss %.3f accuracy %.3f f1 %.3f (f1 %.3f) pr %.3f rc %.3f jac %.3f (jac %.3f ) Runtime %.3f FPS Val %0.3f" % (e, np.mean(train_loss), np.mean(train_accuracy), train_f1, train_f1_2, train_pr, train_rc, train_jac, train_jac_2, np.mean(test_loss), np.mean(test_accuracy), test_f1, test_f1_2, test_pr, test_rc, test_jac, test_jac_2, total, val_fps)
        print(str)
        log_file.write(str + "\n")
        log_file.flush()

        scheduler.step()

        if (e + 1) % 10 == 0:
            torch.save(model.state_dict(), cur_output_folder + "model%04d.th" % e)
